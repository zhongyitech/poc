package com.zhongyi.poc.core.rest.application;

import com.zhongyi.poc.core.rest.dto.ProductDTO;
import com.zhongyi.poc.core.rest.entity.Product;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dong on 2019/3/27.
 */
public interface ProductService {

    List<Product> findAllProducts();

    double buyProduct(int id , int quantity);



}
