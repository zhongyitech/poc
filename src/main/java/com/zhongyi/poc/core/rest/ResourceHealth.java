package com.zhongyi.poc.core.rest;

import com.zhongyi.poc.core.rest.application.HealthService;
import com.zhongyi.poc.core.rest.dto.*;
import com.zhongyi.poc.core.rest.dto.Class;
import com.zhongyi.poc.core.rest.entity.HistoryData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

/**
 * Created by dong on 2019/3/27.
 */

@RestController
@RequestMapping("/health")
public class ResourceHealth {


    public static final String RESULT = "result";
    @Autowired
    HealthService healthService;

    @RequestMapping(path = "/userHistoryData",
            method = GET,
            produces = APPLICATION_JSON_UTF8_VALUE
    )
    public ResponseEntity<Map<String, List<HistoryData>>> userHistoryData(
             @Valid
             @NotNull
             @RequestParam(name = "phoneNumber", required = true) Long userID){


        List<HistoryData> historyDate = healthService.findUserHistoryDataById(userID);

        Map<String,List<HistoryData>> result = new HashMap();
        result.put(RESULT,historyDate);

        return ResponseEntity.ok(result);


    }

    @RequestMapping(path = "/avgHeightAndWeightByGrade",
            method = GET,
            produces = APPLICATION_JSON_UTF8_VALUE
    )
    public ResponseEntity<AvgHeightAndWeight> AvgHeightAndWeightByGrade(
             @Valid
             @NotNull
             @Min(value = 1,message = "最小年级为1")
             @Max(value = 6,message = "最大年级为6")
             @RequestParam(name = "grade", required = true) Integer grade ,

             @NotNull
             @RequestParam(name = "class", required = true) Integer clas


    ){

        HashMap<Integer, AvgHeightAndWeight> gradeAvg = new HashMap<>();
        gradeAvg.put(1,new AvgHeightAndWeight(126.6,26.6));
        gradeAvg.put(2,new AvgHeightAndWeight(132.0,29.9));
        gradeAvg.put(3,new AvgHeightAndWeight(137.2,33.6));
        gradeAvg.put(4,new AvgHeightAndWeight(142.1,37.2));
        gradeAvg.put(5,new AvgHeightAndWeight(148.1,41.9));
        gradeAvg.put(6,new AvgHeightAndWeight(154.5,46.6));

        AvgHeightAndWeight result = gradeAvg.get(grade);

        int tmp = 1;

        if(clas%2==1) tmp = -1;

        result.setHeight(result.getHeight()+tmp*0.78);
        result.setWeight(result.getWeight()-tmp*0.23);


        result.setHeight((double) Math.round(result.getHeight() * 100) / 100);
        result.setWeight((double) Math.round(result.getWeight() * 100) / 100);
//        AvgHeightAndWeight avgHeightAndWeight = new AvgHeightAndWeight();
//        List<HistoryData> listHistoryData = healthService.findUserRecentHistoryDataByGrade(grade);
//
//
//        OptionalDouble avgHeight = listHistoryData
//                .stream()
//                .mapToDouble(data->data.getHeight())
//                .average();
//
//        OptionalDouble avgWeight = listHistoryData
//                .stream()
//                .mapToDouble(data->data.getWeight())
//                .average();

//        avgHeightAndWeight.setHeight(avgHeight.getAsDouble());
//        avgHeightAndWeight.setWeight(avgWeight.getAsDouble());

        return ResponseEntity.ok(result);


    }

//    @RequestMapping(path = "/authenticationByPhoneNumber",
//            method = GET,
//            produces = APPLICATION_JSON_UTF8_VALUE
//    )
    public ResponseEntity<Map<String,Boolean>> authenticationByPhoneNumber(
            @NotNull
            @RequestParam(name = "phoneNumber", required = true) String phoneNumber,

            @NotNull
            @RequestParam(name = "userID", required = true) Integer userID

            ){


        Map<String,Boolean> result = new HashMap<>();
        result.put("result",false);
        AuthenticationResult authenticationResult = new AuthenticationResult(false);
        if(phoneNumber.equals("18002535396")&&userID.intValue()==1){

            result.put("result",true);
            return ResponseEntity.ok(result);
        }

        return ResponseEntity.ok(result);


    }

//    @RequestMapping(path = "/getNumberOfClass",
//            method = GET,
//            produces = APPLICATION_JSON_UTF8_VALUE
//    )
    public ResponseEntity<Map<String,Integer>> getNumberOfClass(

            @NotNull
            @RequestParam(name = "grade", required = true)
            int grade){


        Map<String , Integer> result = new HashMap<>();

        result.put(RESULT,10);

        return ResponseEntity.ok(result);
    }


    @RequestMapping(path = "/saveUserHealthData",
            method = POST,
            produces = APPLICATION_JSON_UTF8_VALUE
    )
    public ResponseEntity<Map<String,Boolean>> saveUserHealthData(
            @NotNull
            @RequestBody
            HistoryDataDto historyData
    ){

        Map<String , Boolean> result = new HashMap<>();

        result.put(RESULT,true);

        HistoryData historyData1 = new HistoryData();
        historyData1.setHeight(historyData.getHeight());
        historyData1.setWeight(historyData.getWeight());
        historyData1.setUserID(
                Long.parseLong(historyData.getPhoneNumber()) );


        healthService.saveHistoryData(historyData1);

        return ResponseEntity.ok(result);
    }

    @RequestMapping(path = "/getGradeAndClassInfo",
            method = GET,
            produces = APPLICATION_JSON_UTF8_VALUE
    )
    public ResponseEntity< Map<String,List<GradeAndClassInfo>>> getGradeAndClassInfo(){

        Class class1 = new Class(5,1);
        Class class2 = new Class(6,2);
        Class class3 = new Class(7,1);
        Class class4 = new Class(8,4);

        List<Class> listClass = new ArrayList<>();

        listClass.add(class1);
        listClass.add(class2);
        listClass.add(class3);
        listClass.add(class4);
        listClass.add(new Class(1,1));
        listClass.add(new Class(2,1));
        listClass.add(new Class(1,2));
        listClass.add(new Class(2,2));
        listClass.add(new Class(3,2));
        listClass.add(new Class(1,3));
        listClass.add(new Class(2,3));
        listClass.add(new Class(3,3));
        listClass.add(new Class(1,4));
        listClass.add(new Class(2,4));
        listClass.add(new Class(3,4));
        listClass.add(new Class(1,5));
        listClass.add(new Class(2,5));
        listClass.add(new Class(3,5));
        listClass.add(new Class(4,5));
        listClass.add(new Class(1,6));
        listClass.add(new Class(2,6));
        listClass.add(new Class(3,6));
        listClass.add(new Class(4,6));
        listClass.add(new Class(5,6));
        listClass.add(new Class(6,6));



        Map<Integer,List<Class>> classByGrade =listClass.stream().collect(Collectors.groupingBy(Class::getGrade));

        Map<String,List<GradeAndClassInfo>> result = new HashMap<>();

        List<GradeAndClassInfo> gradeAndClassInfoList = classByGrade
                                                        .entrySet()
                                                        .stream()
                                                        .map(entry -> new GradeAndClassInfo(entry.getKey(),entry.getValue()))
                                                        .collect(Collectors.toList());
        result.put("result",gradeAndClassInfoList);


        return ResponseEntity.ok(result);

    }


}
