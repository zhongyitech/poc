package com.zhongyi.poc.core.rest.application.impl;

import com.zhongyi.poc.core.rest.application.HealthService;
import com.zhongyi.poc.core.rest.dao.HistoryDataRepository;
import com.zhongyi.poc.core.rest.entity.HistoryData;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by dong on 2019/3/28.
 */
@Service
public class HealthServiceImpl implements HealthService {

    @Autowired
    @Setter
    HistoryDataRepository historyDataRepository;

    public List<HistoryData> findUserHistoryDataById(Long id) {

        return historyDataRepository.findUserHistoryDataById(id);

    }

    @Override
    public List<HistoryData> findUserRecentHistoryDataByGrade(int grade) {

        return historyDataRepository.findUserRecentHistoryDataByGrade(grade);


    }

    @Override
    public void saveHistoryData(HistoryData historyData) {

        //historyDataRepository.saveHistoryData(historyData);

        historyDataRepository.saveHistoryDatanew(
                historyData.getUserID(),
                historyData.getHeight(),
                historyData.getWeight()
        );
    }

    @Override
    public int getNumberOfGrade() {
        return 0;
    }

    @Override
    public int getNumberOfClass(int grade) {
        return 0;
    }

    @Override
    public boolean authenticationByPhoneNumber(int userId, String phoneNumber) {
        return false;
    }


}
