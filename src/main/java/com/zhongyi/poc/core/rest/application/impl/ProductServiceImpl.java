package com.zhongyi.poc.core.rest.application.impl;

import com.zhongyi.poc.core.rest.application.ProductService;
import com.zhongyi.poc.core.rest.dao.ProductRepository;
import com.zhongyi.poc.core.rest.entity.Product;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

/**
 * Created by dong on 2019/3/27.
 */
@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    @Setter
    ProductRepository productRepository ;

    @Override
    public List<Product> findAllProducts() {

        List<Product> result = productRepository.findAll();
        System.out.print(result.size());
        return result;

    }

    @Override
    public synchronized double buyProduct(int id, int quantity) {

        Product product = productRepository.findProductById(id);

        if(product==null) return -1;

        if(product.getQuantity() < quantity) return -1;

        productRepository.updateProductQuantity(id,product.getQuantity()-quantity);

        return quantity * product.getPrix();
    }
}
