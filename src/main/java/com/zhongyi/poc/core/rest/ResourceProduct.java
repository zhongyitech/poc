package com.zhongyi.poc.core.rest;

import com.zhongyi.poc.core.rest.application.ProductService;
import com.zhongyi.poc.core.rest.dto.Mapper;
import com.zhongyi.poc.core.rest.dto.Order;
import com.zhongyi.poc.core.rest.dto.ProductDTO;
import com.zhongyi.poc.core.rest.dto.ProductsDTO;
import com.zhongyi.poc.core.rest.entity.Product;
import jdk.nashorn.internal.runtime.PropertyDescriptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureOrder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;
import static org.springframework.http.MediaType.TEXT_PLAIN_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

/**
 * Created by dong on 2019/3/27.
 */

@RestController
@RequestMapping("/product")
public class ResourceProduct {

    private static final String TEST_PING_MESSAGE = "Serveur ZhongYi OK -";
    @Autowired
    private ProductService productService;

    @RequestMapping(path ="/test",
            method = GET,
            produces = TEXT_PLAIN_VALUE)
    public ResponseEntity<String> testPing(){

        return ResponseEntity.ok(TEST_PING_MESSAGE);
    }



    @RequestMapping(path = "/findAll",
            method = GET,
            produces = APPLICATION_JSON_UTF8_VALUE
    )
    public ResponseEntity<ProductsDTO> findAllProduct(){

        List<Product> result = productService.findAllProducts();

        List<ProductDTO> listDto = result
                .parallelStream()
                .map(p-> Mapper.productToDAO(p))
                .collect(Collectors.toList());

        return ResponseEntity.ok(new ProductsDTO(listDto));


    }

    @RequestMapping(path = "/buyProduct",
            method = POST,
            produces = APPLICATION_JSON_UTF8_VALUE
    )
    public ResponseEntity<Double> buyProduct(
            @Valid
            @NotNull
            @RequestBody Order order){

        Double result = productService.buyProduct(order.getProductID(),order.getQuantity());
        return ResponseEntity.ok(result);

    }


}
