package com.zhongyi.poc.core.rest.dto;

import java.util.List;

/**
 * Created by dong on 2019/3/28.
 */
public class ProductsDTO {

    List<ProductDTO> listProducts;

    public ProductsDTO(List<ProductDTO> listProducts) {
        this.listProducts = listProducts;
    }

    public List<ProductDTO> getListProducts() {
        return listProducts;
    }

    public void setListProducts(List<ProductDTO> listProducts) {
        this.listProducts = listProducts;
    }
}

