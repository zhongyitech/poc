package com.zhongyi.poc.core.rest.application;

import com.zhongyi.poc.core.rest.entity.HistoryData;

import java.util.List;

/**
 * Created by dong on 2019/3/28.
 */
public interface HealthService {

    List<HistoryData> findUserHistoryDataById(Long id);

    List<HistoryData> findUserRecentHistoryDataByGrade(int grade);

    void saveHistoryData(HistoryData historyData);

    int getNumberOfGrade();

    int getNumberOfClass(int grade);

    boolean authenticationByPhoneNumber(int userId, String phoneNumber);


}
