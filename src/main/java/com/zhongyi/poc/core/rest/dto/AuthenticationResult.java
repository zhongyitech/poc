package com.zhongyi.poc.core.rest.dto;

/**
 * Created by dong on 2019/3/29.
 */
public class AuthenticationResult {


    boolean result;

    public AuthenticationResult(boolean result) {
        this.result = result;
    }

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

}
