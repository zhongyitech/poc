package com.zhongyi.poc.core.rest.dao;

import com.zhongyi.poc.core.rest.entity.Product;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by dong on 2019/3/27.
 */
@Repository
@Mapper
public interface ProductRepository {


    List<Product> findAll();

    Product findProductById(@Param("id") Integer id);

    void updateProductQuantity(
            @Param("id") Integer id ,
            @Param("quantity") Integer quantity);


}
