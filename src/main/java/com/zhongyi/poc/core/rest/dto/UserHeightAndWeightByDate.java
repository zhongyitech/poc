package com.zhongyi.poc.core.rest.dto;

import java.util.Date;

/**
 * Created by dong on 2019/3/27.
 */
public class UserHeightAndWeightByDate {

    int userID;
    double height;
    double weight;
    Date date;

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
