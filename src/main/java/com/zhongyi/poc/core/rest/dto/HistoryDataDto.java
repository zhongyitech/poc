package com.zhongyi.poc.core.rest.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by dong on 2019/3/30.
 */
@Getter
@Setter
public class HistoryDataDto {

    String phoneNumber;
    double height;
    double weight;

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }
}
