package com.zhongyi.poc.core.rest.entity;

import lombok.Data;

import java.util.Date;

/**
 * Created by dong on 2019/3/28.
 */

@Data
public class HistoryData {

    Long userID;
    double height;
    double weight;
    Date date;

    public Long getUserID() {
        return userID;
    }

    public void setUserID(Long userID) {
        this.userID = userID;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
