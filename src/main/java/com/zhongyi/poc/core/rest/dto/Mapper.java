package com.zhongyi.poc.core.rest.dto;

import com.zhongyi.poc.core.rest.entity.Product;

/**
 * Created by dong on 2019/3/28.
 */
public class Mapper {

    public static ProductDTO productToDAO(Product product) {


        ProductDTO productDTO = new ProductDTO();

        productDTO.setId(product.getId());
        productDTO.setPhoto(product.getPhoto());
        productDTO.setName(product.getName());
        productDTO.setPrix(product.getPrix());
        productDTO.setQuantity(product.getQuantity());
        productDTO.setPosition(product.getPosition());


        return productDTO;

    }

}
