package com.zhongyi.poc.core.rest.dao;

import com.zhongyi.poc.core.rest.entity.HistoryData;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
@Mapper
public interface HistoryDataRepository {

    List<HistoryData> findUserHistoryDataById(@Param("id") Long id);

    List<HistoryData> findUserRecentHistoryDataByGrade(@Param("grade") int grade);

    void saveHistoryData(@Param("historyData") HistoryData historyData);

    void saveHistoryDatanew(@Param("id") Long id,
                            @Param("height") double height,
                            @Param("weight") double weight);


}
