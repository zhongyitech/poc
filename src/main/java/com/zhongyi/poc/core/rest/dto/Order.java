package com.zhongyi.poc.core.rest.dto;

import javax.validation.constraints.NotNull;

import javax.validation.constraints.Positive;

/**
 * Created by dong on 2019/3/27.
 */
public class Order {

    @NotNull
    @Positive
    int productID;

    @Positive
    @NotNull
    int quantity;

    public int getProductID() {
        return productID;
    }

    public void setProductID(int productID) {
        this.productID = productID;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
