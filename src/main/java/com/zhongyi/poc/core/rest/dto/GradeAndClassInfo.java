package com.zhongyi.poc.core.rest.dto;

import java.util.List;

/**
 * Created by dong on 2019/3/30.
 */
public class GradeAndClassInfo {

    int grade;

    List<Class> clas;

    public GradeAndClassInfo(int grade, List<Class> clas) {
        this.grade = grade;
        this.clas = clas;
    }

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }

    public List<Class> getClas() {
        return clas;
    }

    public void setClas(List<Class> clas) {
        this.clas = clas;
    }
}
