package com.zhongyi.poc.core.rest.dto;

/**
 * Created by dong on 2019/3/29.
 */
public class Class {

    int name;
    int grade;

    public Class(int name, int grade) {
        this.name = name;
        this.grade = grade;
    }

    public int getName() {
        return name;
    }

    public void setName(int name) {
        this.name = name;
    }

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }
}
