package com.zhongyi.poc.core.rest.dto;

/**
 * Created by dong on 2019/3/27.
 */
public class AvgHeightAndWeight {


    double height;
    double weight;

    public AvgHeightAndWeight(double height, double weight) {
        this.height = height;
        this.weight = weight;
    }


    public AvgHeightAndWeight() {
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }
}
