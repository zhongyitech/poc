package com.zhongyi.poc.core.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.validation.ConstraintViolationException;
import java.io.IOException;
import java.net.URISyntaxException;

@ControllerAdvice
@Order
public class WebExceptionHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(WebExceptionHandler.class);

    @ExceptionHandler({IllegalArgumentException.class, HttpMessageNotReadableException.class})
    public ResponseEntity defaultErrorHandler(IllegalArgumentException e) {
        LOGGER.error(
                "Wrong arguments given to request",
                e);
        return new ResponseEntity(HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity constraintError(ConstraintViolationException e) {
        LOGGER.error(
                "request validation failed",
                e);
        return new ResponseEntity(HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(IOException.class)
    public ResponseEntity inputOutputError(IOException e) {
        LOGGER.error(
                "input/output error",
                e);
        return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity argumentNotValid(MethodArgumentNotValidException e) {
        LOGGER.error(
                "method argument not valid error",
                e);
        return new ResponseEntity(HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(URISyntaxException.class)
    public ResponseEntity uriSyntaxError(URISyntaxException e) {
        LOGGER.error(
                "uri syntax error",
                e);
        return new ResponseEntity(HttpStatus.BAD_GATEWAY);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity unknownError(Exception e) {
        LOGGER.error(
                "unknown error: ",
                e);
        return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
